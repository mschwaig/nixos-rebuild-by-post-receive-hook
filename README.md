# nixos-rebuild-by-post-receive-hook

## What is this

This script is a small wrapper around `nixos-rebuild` that when called from a post-receive git hook can be used to remotely deploy updates to NixOS machines that are configured from a Nix Flake located in a non-bare git repo located at `/etc/nixos`.

Together with repo mirroring as provided by platforms like GitHub or GitLab this can make those platforms automatically push updates to your master branch to the deployment target.

## Additional setup required

### On the target

To make that work the script has to be invoked from a post-receive hook that has to be manually placed in the destination repo.

The post-receive hook script at `/etc/nixos/.git/hooks/post-receive` should have the following contents:

```sh
#!/bin/sh

# ! this script and the program it invokes !
# !      do not work with a bare repo      !

# apply any new system configuration on the master branch
nixos-rebuild-by-post-receive-hook
```

Additionally the following settings are also necessary in the target repo on the destination host:

```
$ git config core.worktree /etc/nixos
$ git config receive.denycurrentbranch updateInstead
```

Those commands were originally listed in the following guide:
https://buddy.works/blog/how-deploy-projects-with-git

I have adopted the `receive.denycurrentbranch` setting so that it updates the working directory as well, but fails if the working directory has changes. If that setting was set to `ignore` the working directory would not update, which breaks this use case. See:
https://stackoverflow.com/a/28262104/2066744

### At the source

TODO: write down some useful info - got it working with GitLab
