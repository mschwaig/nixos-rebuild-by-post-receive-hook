

{
  description = "flake auto update for gitlab";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
  utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };
      branch-to-follow = "master";
    in {

# TODO: find a sensible way to create a record of this operation,
# or even better to create a record that points to the one nixos might already be creating
# TODO: add [${networking.hostName}] of machine to be built

      defaultPackage = pkgs.writeScriptBin "nixos-rebuild-by-post-receive-hook" ''
        #!${pkgs.stdenv.shell}

        if [ -z "$GIT_DIR" ]; then
            echo "ERROR: this script has to be invoked from a git post receive hook"
            exit 1
        fi

        # set working directory to the git workspace directory
        cd $GIT_DIR/..

        # For picking out if a specific branch has been updated see:
        # https://stackoverflow.com/a/13057643/2066744 while read oldrev newrev refname
        while read oldrev newrev refname
        do
            branch=$(${pkgs.git}/bin/git rev-parse --symbolic --abbrev-ref $refname)

            if [ "${branch-to-follow}" = "$branch" ]; then

                # Since nixos-rebuild will invoke git we have to
                # unset GIT_DIR.
                # See: https://stackoverflow.com/a/4100577/2066744
                unset GIT_DIR

                nixos-rebuild switch --flake .

                if [ $? -eq 0 ]; then
                    echo "successfully switched to new system configuration"
                    exit 0
                else
                    echo "ERROR: failed to switch to new system configuration"
                    exit 1
                fi
            fi
        done
      '';
    });
  }
